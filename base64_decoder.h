#pragma once

#include <iostream>
#include <string>

using namespace std;

char base64Chars[] = {
	'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
	'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
	'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
	'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
	'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'
};

unsigned char convertCharToByte(char c){
	for(unsigned char i = 0; i < 64; i++){
		if(c == base64Chars[i]){
			return i;
		}
	}
	return -1;
}

void convert4CharsTo3Bytes(char* c, unsigned char* b){
	unsigned char uncompressedBytes[4];
	for(unsigned int i = 0; i < 4; i++){
		//cout << c[i] << '\n';
		uncompressedBytes[i] = convertCharToByte(c[i]);
		//cout << uncompressedBytes[i] << '\n';
	}
	
	b[0] = (uncompressedBytes[0] << 2) + (uncompressedBytes[1] >> 4);
	b[1] = (uncompressedBytes[1] << 4) + (uncompressedBytes[2] >> 2);
	b[2] = (uncompressedBytes[2] << 6) + uncompressedBytes[3];
}

unsigned char* stringToByteArray(string input, int* size){
	unsigned int length = input.length();
	*size = (length / 4) * 3;
	unsigned char* byteArray = new unsigned char[*size];
	unsigned int ctr = 0;	
	for(unsigned int i = 0; i < length; i += 4){
		convert4CharsTo3Bytes(&input.at(i), &byteArray[ctr]);
		ctr += 3;		
	}	

	return byteArray;
} 

void deleteByteArray(unsigned char* array){
	delete array;
}


